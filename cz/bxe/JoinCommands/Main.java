package cz.bxe.JoinCommands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin implements Listener {

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, this);
        getCommand("jcreload").setExecutor(this);
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
    }

    public void onDisable(){}

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        for(String cmd : getConfig().getStringList("joinCommandPlayer")){
            Bukkit.dispatchCommand(p, cmd);
        }
        for(String cmd : getConfig().getStringList("joinCommandConsole")){
            cmd = cmd.replace("%player%", p.getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("jcreload")){
            if(sender.hasPermission("joincommads.reload")){
                reloadConfig();
                sender.sendMessage("§aJoinCommands reloaded...");
            } else {
                sender.sendMessage("§cNo permission...");
            }
        }
        return false;
    }


}
